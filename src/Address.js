import React, { PureComponent } from "react";
import { Row, Col, Table, Pagination, Icon } from "react-materialize";
import "./Address.css";
import { validateAddress, changeEndianness, convertHash, sleep } from "./utils";

import { Buffer } from "buffer";

//GRPC imports
// import * as pb from "./bchrpc/bchrpc_pb_service";
import {
  GetAddressTransactionsRequest,
  // GetAddressTransactionsResponse,
  Transaction,
  MempoolTransaction,
  TransactionFilter,
  SubscribeTransactionsRequest,
  // TransactionNotification,
  GetAddressUnspentOutputsRequest,
  SubscribeBlocksRequest,
  // BlockNotification,
  // GetAddressUnspentOutputsResponse,
} from "./bchrpc/bchrpc_pb";
import { BrowserHeaders } from "browser-headers";

const headers = new BrowserHeaders();
headers.set("Access-Control-Allow-Origin", "*");
headers.set("Access-Control-Allow-Credentials", "true");
headers.set(
  "Access-Control-Allow-Headers",
  "Origin, Content-Type, Authorization, x-id, Content-Length, X-Requested-With, Date , Vary, Access-Control-Allow-Origin, Access-Control-Allow-Credentials"
);
headers.set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");

class Address extends PureComponent {
  constructor(props) {
    super(props);

    let addressValidation = validateAddress(props.address);
    if (addressValidation.isValid) {
      props.setSearchAddress(addressValidation.address);
    }

    this.state = {
      server: props.server,
      // client: new pb.bchrpcClient(props.server),
      client: props.client,
      address: addressValidation.address,
      address_error: addressValidation.error,
      confirmed_transactions: [],
      unconfirmed_transactions: [],
      balance: 0,
      nb_skip: 0,
      nb_fetch: 10,
      active_page: 1,
      last_page: false,
      best_block_height: 0,
    };

    this.getAddressUnspentOutputs = this.getAddressUnspentOutputs.bind(this);
    this.getAddressTransactions = this.getAddressTransactions.bind(this);
    this.subscribeTransactions = this.subscribeTransactions.bind(this);
    this.subscribeBlockChain = this.subscribeBlockChain.bind(this);
    this.handlePages = this.handlePages.bind(this);
  }

  async componentDidMount() {
    await this.getAddressUnspentOutputs();
    await this.getAddressTransactions();
    await this.subscribeTransactions();
    await this.subscribeBlockChain();
  }

  componentWillUnmount() {
    try {
      console.log("cancel preexpisting transaction subscription stream");
      this.state.stream.cancel();
    } catch (error) {
      console.log("error closing transaction stream", error);
    }

    try {
      console.log("cancel preexpisting block subscription stream");
      this.state.block_stream.cancel();
    } catch (error) {
      console.log("error closing block stream", error);
    }
  }

  async componentDidUpdate(prevProps) {
    if (this.props.server !== prevProps.server || this.props.client !== prevProps.client) {
      this.setState({
        server: this.props.server,
        client: this.props.client,
      });
      await this.getAddressUnspentOutputs();
      await this.getAddressTransactions();
      await this.subscribeTransactions();
      await this.subscribeBlockChain();
    }
    if (this.props.address !== prevProps.address) {
      let addressValidation = validateAddress(this.props.address);
      if (addressValidation.isValid) {
        this.setState({
          address: addressValidation.address,
          unconfirmed_transactions: [],
          confirmed_transactions: [],
          balance: 0,
          nb_skip: 0,
          active_page: 1,
        });
        this.props.setSearchAddress(addressValidation.address);
        await this.getAddressUnspentOutputs();
        await this.getAddressTransactions();
        await this.subscribeTransactions();
        await this.subscribeBlockChain();
      } else {
        this.setAddressError(addressValidation.error);
      }
    }
  }

  setAddressError(error) {
    this.setState({ address_error: error });
  }

  // get UTXO set for current address to calculate confirmed balance
  async getAddressUnspentOutputs() {
    let getAddressUnspentOutputsRequest = new GetAddressUnspentOutputsRequest();
    getAddressUnspentOutputsRequest.setAddress(this.props.address);

    console.log("gRPC getAddressUnspentOutputs:", this.props.address);

    await this.props.client.getAddressUnspentOutputs(getAddressUnspentOutputsRequest, headers, function (error, responseMessage) {
      if (error) {
        console.log("Error: " + error.code + ": " + error.message);
      } else {
        let outputList = responseMessage.getOutputsList();
        let balance = 0;
        outputList.forEach((output) => {
          balance += output.getValue();
        });
        this.setState({ balance: balance });
      }
    }.bind(this));
  }

  // get confirmed and unconfirmed transaction to populate transaction tables and calculate unconfirmed balance
  async getAddressTransactions(selectedPage) {
    let nb_skip = this.state.nb_skip;
    let nb_fetch = this.state.nb_fetch;
    if (this.state.confirmed_transactions.length === 0) {
      nb_fetch *= 2;
    }

    if (selectedPage !== undefined) {
      if (selectedPage * this.state.nb_fetch + this.state.nb_fetch - this.state.confirmed_transactions.length > 0) {
        nb_fetch = selectedPage * this.state.nb_fetch + this.state.nb_fetch - this.state.confirmed_transactions.length;
      }
    }

    let getAddressTransactionsRequest = new GetAddressTransactionsRequest();
    getAddressTransactionsRequest.setAddress(this.props.address);
    getAddressTransactionsRequest.setNbSkip(nb_skip);
    getAddressTransactionsRequest.setNbFetch(nb_fetch);

    console.log("gRPC getAddressTransactions:", this.props.address);
    console.log("getAddressTransactions nb_skip:", nb_skip);
    console.log("getAddressTransactions nb_fetch:", nb_fetch);

    await this.props.client.getAddressTransactions(getAddressTransactionsRequest, headers,
      function (error, responseMessage) {
        if (error) {
          console.log("getAddressTransactions error: " + error.code + ": " + error.message);
        } else {
          let confirmedResponse = responseMessage.getConfirmedTransactionsList();
          let confirmedTransactions = this.state.confirmed_transactions.concat(confirmedResponse);
          this.setState({
            unconfirmed_transactions: responseMessage.getUnconfirmedTransactionsList(),
            confirmed_transactions: confirmedTransactions,
            nb_skip: this.state.nb_skip + nb_fetch,
            last_page:
              confirmedResponse.length % nb_fetch !== 0 || confirmedResponse.length === 0
                ? true
                : false,
          });
        }
      }.bind(this)
    );
  }

  // subscribe live transaction stream to get new mempool transactions and update unconfirmed balance
  async subscribeTransactions() {
    let filter = new TransactionFilter();
    filter.addAddresses(this.props.address);

    let subscribreTransactionRequest = new SubscribeTransactionsRequest();
    subscribreTransactionRequest.setIncludeMempool(true);
    // subscribreTransactionRequest.setIncludeInBlock(true);
    subscribreTransactionRequest.setSubscribe(filter);

    filter.getAddressesList().forEach((address) => {
      console.log("gRPC subscribeTransactions: " + address);
    });

    if (this.state.stream) {
      console.log("cancel preexpisting transaction subscription stream");
      try {
        this.state.stream.cancel();
      } catch (error) {
        console.log("error closing stream", error);
      }
    }

    this.setState({
      stream: this.props.client.subscribeTransactions(subscribreTransactionRequest, headers),
      address_error: "",
    });

    this.state.stream.on("data", function (transactionNotification) {

      if (transactionNotification.hasUnconfirmedTransaction()) {
        let mempooltx = new MempoolTransaction();
        mempooltx = transactionNotification.getUnconfirmedTransaction();
        console.log("mempool tx notif: " + changeEndianness(Buffer.from(mempooltx.getTransaction().getHash_asU8()).toString("hex")));

        let unconfirmed_transactions = [...this.state.unconfirmed_transactions];
        unconfirmed_transactions.push(mempooltx);

        this.setState({
          unconfirmed_transactions: unconfirmed_transactions,
        });
      }
    }.bind(this)
    );
    this.state.stream.on("status", function (status) {
      console.log("grpc status: " + status["details"]);
      this.setAddressError("grpc status: " + status["details"]);
    }.bind(this)
    );
    this.state.stream.on("end", async function (status) {
      console.log("grpc end: " + status["details"]);
      await sleep(5000);
      await this.getAddressUnspentOutputs();
      await this.getAddressTransactions(this.state.active_page);
      await this.subscribeTransactions();
      await this.subscribeBlockChain();
    }.bind(this)
    );
  }

  // subscribe live block stream. new block triggers update of balances and transaction tables
  async subscribeBlockChain() {
    const subscribreBlocksRequest = new SubscribeBlocksRequest();

    if (this.state.block_stream) {
      console.log("cancel preexpisting blocks subscription stream");
      try {
        this.state.block_stream.cancel();
      } catch (error) {
        console.log("error closing blocks stream", error);
      }
    }

    this.setState({
      block_stream: this.props.client.subscribeBlocks(subscribreBlocksRequest, headers),
    });

    this.state.block_stream.on("data", async function (blockNotification) {
      let blockInfo = blockNotification.getBlock();
      console.log("subscribeBlockChain new block:", blockInfo.getHeight(), convertHash(blockInfo.getHash_asU8()));

      this.setState({best_block_height: blockInfo.getHeight()});

      // new block triggers fetching of newly confirmed txs since blockheight of the most recent confirmed tx + all mempool txs
      let getAddressTransactionsRequest = new GetAddressTransactionsRequest();
      getAddressTransactionsRequest.setAddress(this.props.address);
      getAddressTransactionsRequest.setHeight(this.state.confirmed_transactions[0].getBlockHeight() + 1);

      console.log("getAddressTransactions since:", this.state.confirmed_transactions[0].getBlockHeight() + 1, this.props.address);

      await this.props.client.getAddressTransactions(getAddressTransactionsRequest, headers,
        function (error, responseMessage) {
          if (error) {
            console.log("getAddressTransactions error: " + error.code + ": " + error.message);
          } else {
            let newConfirmedTransactions = responseMessage.getConfirmedTransactionsList();
            let confirmedTransactions = newConfirmedTransactions.concat(this.state.confirmed_transactions)
            this.setState({
              unconfirmed_transactions: responseMessage.getUnconfirmedTransactionsList(),
              confirmed_transactions: confirmedTransactions,
              nb_skip: confirmedTransactions.length,
            });
          }
        }.bind(this)
      );

      await this.getAddressUnspentOutputs();

    }.bind(this));
    this.state.block_stream.on("status", function (status) {
      console.log("subscribeBlockChain status:", status);
    });
    this.state.block_stream.on("end", function (status) {
      console.log("subscribeBlockChain end:", status);
    });
  }



  async handlePages(select) {
    console.log("selected page:", select);
    console.log("tx count:", this.state.confirmed_transactions.length);
    console.log("nb_skip:", this.state.nb_skip);
    console.log("nb_fetch:", this.state.nb_fetch);
    console.log("last_page:", this.state.last_page);

    let nb_fetch = this.state.nb_fetch;

    if (select * nb_fetch + nb_fetch > this.state.confirmed_transactions.length && !this.state.last_page) {
      await this.getAddressTransactions(select);
      this.setState({ active_page: select });
    } else {
      this.setState({
        active_page: select,
      });
    }
  }

  calculatePages() {
    let txCount = this.state.confirmed_transactions.length;
    // let pageCount = this.state.last_page ? (txCount / this.state.nb_fetch).toFixed(0)  : (txCount / this.state.nb_fetch) + 1;
    let pageCount = this.state.last_page
      ? Math.ceil(txCount / this.state.nb_fetch)
      : Math.ceil(txCount / this.state.nb_fetch);
    // : Math.ceil(txCount / this.state.nb_fetch) + 1;
    // console.log("calculated pages:", pageCount);
    return Number(pageCount);
  }

  parseMempoolTransaction(transaction) {
    let utx = new MempoolTransaction();
    utx = transaction;
    let addedTime = utx.getAddedTime();

    let tx = new Transaction();
    tx = utx.getTransaction();
    let parsedTx = this.parseTransaction(tx);

    parsedTx.timestamp = addedTime;

    return parsedTx;
  }

  parseTransaction(transaction) {
    let tx = new Transaction();
    tx = transaction;

    let txid = changeEndianness(Buffer.from(tx.getHash_asU8()).toString("hex"));
    let confirmations = tx.getConfirmations();
    let timestamp = tx.getTimestamp();
    let blockheight = tx.getBlockHeight();

    let balance = 0;
    let inputList = tx.getInputsList();
    for (let i = 0; i < inputList.length; i++) {
      let input = new Transaction.Input();
      input = inputList[i];
      if (input.getAddress() === this.props.address) {
        balance -= input.getValue();
      }
    }

    let outputList = tx.getOutputsList();
    for (let i = 0; i < outputList.length; i++) {
      let output = new Transaction.Output();
      output = outputList[i];
      if (output.getAddress() === this.props.address) {
        balance += output.getValue();
      }
    }

    return {
      txid: txid,
      confirmations: confirmations,
      timestamp: timestamp,
      blockheight: blockheight,
      balance: balance,
    };
  }

  calcUnconfirmedBalance() {
    let balance = 0;

    this.state.unconfirmed_transactions.forEach((utx) => {
      let tx = utx.getTransaction();
      let parsedTx = this.parseTransaction(tx);
      balance += parsedTx.balance;
    });

    return balance;
  }

  render() {
    return (
      <div>
        <div>
          <Row className="address-row">
            <Col s={12} className="black-text center">
              <div className="address">{this.state.address}</div>
            </Col>
          </Row>
          <Row>
            <Col s={4} m={2} l={2} offset="s4 m5 l5" className="black-text">
              <div className="input-field label-field">
                <input
                  placeholder="Label"
                  value={this.props.label}
                  id="textinput-label"
                  type="text"
                  className="center"
                  onChange={this.props.handleAddressLabel.bind(this)}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col s={12} className="black-text center">
              <span className="address-error">{this.state.address_error}</span>
            </Col>
          </Row>
          <Row>
            <Col s={12} m={6} l={6} className="black-text">
              <div className="balance-label left-align">Balance:</div>
              <div className="balance left-align">
                {" "}
                {(this.state.balance / 100000000).toFixed(8)} BCH
              </div>
            </Col>
            <Col s={12} m={6} l={6} className="grey-text">
              <div className="balance-label right-align">Unconfirmed:</div>
              <div className="balance right-align">
                {this.calcUnconfirmedBalance() >= 0
                  ? "+" + (this.calcUnconfirmedBalance() / 100000000).toFixed(8)
                  : (this.calcUnconfirmedBalance() / 100000000).toFixed(8)}{" "}
                BCH
              </div>
            </Col>
          </Row>
        </div>
        {this.state.unconfirmed_transactions.length > 0 && (
          <div>
            <Table striped responsive>
              <thead>
                <tr>
                  {/* <th data-field="blockheight">Block Height</th> */}
                  <th data-field="blockheight">Timestamp</th>
                  <th data-field="txid">
                    TXID ({this.state.unconfirmed_transactions.length})
                  </th>
                  <th data-field="amount">Amount</th>
                </tr>
              </thead>
              <tbody>
                {[...this.state.unconfirmed_transactions]
                  .reverse()
                  .map((tx) => {
                    let parsedTx = this.parseMempoolTransaction(tx);
                    return (
                      <tr key={parsedTx.txid}>
                        {/* <td className="mono-font grey-text">
                          {parsedTx.blockheight}
                        </td> */}
                        <td className="mono-font grey-text">
                          {new Date(parsedTx.timestamp * 1000)
                            .toISOString()
                            .slice(0, 10) +
                            " " +
                            new Date(parsedTx.timestamp * 1000)
                              .toISOString()
                              .slice(11, 19)}
                        </td>
                        <td className="mono-font grey-text">{parsedTx.txid}</td>
                        <td className="mono-font grey-text">
                          {parsedTx.balance > 0
                            ? "+" +
                            (parsedTx.balance / 100000000).toFixed(8) +
                            " BCH"
                            : (parsedTx.balance / 100000000).toFixed(8) +
                            " BCH"}
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </div>
        )}
        {/* <div className="divider"></div> */}
        <div className="center">
          <Pagination className="pagination-top"
            activePage={this.state.active_page}
            items={this.calculatePages()}
            leftBtn={<Icon>chevron_left</Icon>}
            rightBtn={<Icon>chevron_right</Icon>}
            onSelect={this.handlePages.bind(this)}
          />
        </div>
        <div>
          <Table striped responsive>
            <thead>
              <tr>
                <th data-field="blockheight">Block Height<br/>(confirmations)</th>
                <th data-field="blockheight">Timestamp</th>
                <th data-field="txid">
                  TXID ({this.state.confirmed_transactions.length})
                </th>
                <th data-field="amount">Amount</th>
              </tr>
            </thead>
            <tbody>
              {this.state.confirmed_transactions
                .slice(
                  (this.state.active_page - 1) * 10,
                  this.state.active_page * 10
                )
                .map((tx) => {
                  let parsedTx = this.parseTransaction(tx);

                  return (
                    <tr key={parsedTx.txid}>
                      <td className="mono-font">
                        { parsedTx.blockheight} ({this.state.best_block_height === 0 
                                                    ? parsedTx.confirmations 
                                                    : (this.state.best_block_height - parsedTx.blockheight) +1})
                      </td>
                      <td className="mono-font">
                        {new Date(parsedTx.timestamp * 1000)
                          .toISOString()
                          .slice(0, 10) +
                          " " +
                          new Date(parsedTx.timestamp * 1000)
                            .toISOString()
                            .slice(11, 19)}
                      </td>
                      <td className="mono-font">{parsedTx.txid}</td>
                      <td className="mono-font">
                        {parsedTx.balance > 0
                          ? "+" +
                          (parsedTx.balance / 100000000).toFixed(8) +
                          " BCH"
                          : (parsedTx.balance / 100000000).toFixed(8) + " BCH"}
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        </div>
        <div className="center">
          <Pagination
            activePage={this.state.active_page}
            items={this.calculatePages()}
            leftBtn={<Icon>chevron_left</Icon>}
            rightBtn={<Icon>chevron_right</Icon>}
            onSelect={this.handlePages.bind(this)}
          />
        </div>
        <div className="center" key={this.props.server}>
          gRPC server: {this.props.server}
        </div>
      </div>
    );
  }
}

export default Address;
