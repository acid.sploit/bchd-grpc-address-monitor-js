import React, { PureComponent } from "react";
// import './Address.css';
// import 'materialize-css'
import M from "materialize-css";
import { Container, Select, Row, Col } from "react-materialize";
import "./Nav.css";
import Live from './Live';

class Nav extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      server: props.server,
      client: props.client,
      networks: props.networks,
    };
  }

  componentDidMount() {
    M.AutoInit();
  }

  componentDidUpdate(prevProps) {}

  render() {
    return (
      <div>
        <div className="navbar-fixed">
          <nav className="grey lighten-2">
          <a href="#!" data-target="slide-out" className="sidenav-trigger hide-on-med-and-up"><i className="material-icons black-text">menu</i></a>
            <Container>
              <Row className="nav-row">
                <Col s={12} m={6} l={5} offset="l1" className="brand-col">
                  {/* <span className="brand-logo black-text">
                    BCHD Address Monitor
                  </span> */}
                  {/* <a href="/" className="brand-logo black-text"> */}
                  <a href="/" className="brand black-text">
                    BCHD Address Monitor
                  </a>
                </Col>
                <Col s={12} m={6} l={6}>
                  <Select
                    id="select-network"
                    multiple={false}
                    onChange={this.props.handleServer.bind(this)}
                    options={{
                      classes: "dropdown right",
                      dropdownOptions: {
                        alignment: "right",
                        autoTrigger: true,
                        closeOnClick: true,
                        constrainWidth: false,
                        coverTrigger: false,
                        hover: false,
                        inDuration: 150,
                        onCloseEnd: null,
                        onCloseStart: null,
                        onOpenEnd: null,
                        onOpenStart: null,
                        outDuration: 250,
                      },
                    }}
                    value={this.props.server}
                  >
                    {this.state.networks["mainnet"].map((server) => {
                      // console.log("mainnet - " + server)
                      return (
                        <option
                          key={server}
                          className="black-text"
                          value={server}
                        >
                          mainnet - {server}
                        </option>
                      );
                    })}
                    {this.state.networks["testnet"].map((server) => {
                      // console.log("mainnet - " + server)
                      return (
                        <option
                          key={server}
                          className="black-text"
                          value={server}
                        >
                          testnet - {server}
                        </option>
                      );
                    })}
                  </Select>
                </Col>
              </Row>
              <Live server={this.state.server} client={this.state.client} />
            </Container>
          </nav>
        </div>
      </div>
    );
  }
}

export default Nav;
