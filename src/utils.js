import { Buffer } from 'buffer';
import { decodeCashAddress, decodeCashAddressFormatWithoutPrefix } from '@bitauth/libauth'

export function validateAddress(address) {
    console.log("validate address: ", address);
    if (address.slice(0, 12) === 'bitcoincash:' || address.slice(0, 13) === 'simpleledger:' || address.slice(0, 8) === 'testnet:') {
        let decodedCashAddress = decodeCashAddress(address);
        if (typeof (decodedCashAddress) === 'object') {
            if (address.slice(0, 12) === 'bitcoincash:') {
                address = address.slice(0, 12);
            }
            if (address.slice(0, 13) === 'simpleledger:') {
                address = address.slice(0, 13);
            }
            if (address.slice(0, 8) === 'testnet:') {
                address = address.slice(0, 8);
            }
            return {
                isValid: true,
                address: address,
                error: "",
            }
        } else if (typeof (decodedCashAddress) === 'string') {
            return {
                isValid: false,
                address: "",
                error: decodedCashAddress,
            }
        }
    } else {
        let decodedCashAddress = decodeCashAddressFormatWithoutPrefix(address);
        if (typeof (decodedCashAddress) === 'object') {
            return {
                isValid: true,
                address: address,
                error: "",
            }

        } else if (typeof (decodedCashAddress) === 'string') {
            return {
                isValid: false,
                address: "",
                error: decodedCashAddress,
            }
        }
    }
}

export function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

export const convertUint8ArrToHex = (Uint8Array) => {
    return Buffer.from(Uint8Array).toString("hex");
}

export const changeEndianness = (string) => {
    const result = [];
    let len = string.length - 2;
    while (len >= 0) {
        result.push(string.substr(len, 2));
        len -= 2;
    }
    return result.join('');
}

export const convertHash = (Uint8Array) => {
    let toHex = Buffer.from(Uint8Array).toString("hex");
    let reversed = changeEndianness(toHex);

    return reversed;
}