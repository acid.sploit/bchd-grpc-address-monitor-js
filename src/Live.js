import React, { useState, useEffect } from "react";
import { Row, Col } from "react-materialize";
import "./Live.css";

// import * as pb from "./bchrpc/bchrpc_pb_service";
import {
  GetMempoolInfoRequest,
  GetBlockchainInfoRequest,
  SubscribeTransactionsRequest,
  TransactionFilter,
  TransactionNotification,
  SubscribeBlocksRequest,
  BlockNotification,
} from "./bchrpc/bchrpc_pb";

import { BrowserHeaders } from "browser-headers";
const headers = new BrowserHeaders();
headers.set("Access-Control-Allow-Origin", "*");
headers.set("Access-Control-Allow-Credentials", "true");
headers.set(
  "Access-Control-Allow-Headers",
  "Origin, Content-Type, Authorization, x-id, Content-Length, X-Requested-With, Date , Vary, Access-Control-Allow-Origin, Access-Control-Allow-Credentials"
);
headers.set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");

function Live(props) {
  //   let server = props.server;
  let client = props.client;

  const [blockHeight, setBlockHeight] = useState(0);
  const [mempoolSize, setMempoolSize] = useState(0);
  const [mempoolBytes, setMempoolBytes] = useState(0);

  useEffect(() => {
    async function getChainInfo() {
      const getBlockchainInfoRequest = new GetBlockchainInfoRequest();

      await client.getBlockchainInfo(
        getBlockchainInfoRequest,
        headers,
        function (error, responseMessage) {
          if (error) {
            console.log("Error: " + error.code + ": " + error.message);
          } else {
            var chain = responseMessage.toObject();
            // console.log("chaininfo", chain);
            setBlockHeight(chain.bestHeight);
          }
        }
      );
    }
    getChainInfo();
  }, [client]);

  useEffect(() => {
    async function getMempoolInfo() {
      const getMempoolInfoRequest = new GetMempoolInfoRequest();

      await client.getMempoolInfo(
        getMempoolInfoRequest,
        headers,
        function (error, responseMessage) {
          if (error) {
            console.log("Error: " + error.code + ": " + error.message);
          } else {
            var mempool = responseMessage.toObject();
            // console.log("mempoolinfo", mempool);

            setMempoolSize(mempool.size);
            setMempoolBytes(mempool.bytes);
          }
        }
      );
    }
    getMempoolInfo();
  }, [blockHeight, client]);

  useEffect(() => {
    const subscribreBlocksRequest = new SubscribeBlocksRequest();
    let stream = client.subscribeBlocks(subscribreBlocksRequest, headers);

    stream.on("data", function (message) {
      let block = new BlockNotification();
      block = message.toObject();
      // console.log("block", block);

      setMempoolSize(0);
      setMempoolBytes(0);
      setBlockHeight(block.block.height);
    });
    stream.on("status", function (status) {
      console.log(status);
    });
    stream.on("end", function (status) {
      console.log(status);
    });

    return function cleanup() {
      stream.cancel();
    };
  }, [client]);

  useEffect(() => {
    let filter = new TransactionFilter();
    filter.setAllTransactions(true);

    let subscribreTransactionRequest = new SubscribeTransactionsRequest();
    subscribreTransactionRequest.setIncludeMempool(true);
    subscribreTransactionRequest.setIncludeInBlock(false);

    subscribreTransactionRequest.setSubscribe(filter);

    let stream = client.subscribeTransactions(subscribreTransactionRequest, headers);

    stream.on("data", function (message) {
      let tx = new TransactionNotification();
      tx = message.toObject();

      setMempoolSize((mempoolSize) => mempoolSize + 1);
      setMempoolBytes((mempoolBytes) => mempoolBytes + tx.unconfirmedTransaction.transaction.size);
    });
    stream.on("status", function (status) {
      console.log(status);
    });
    stream.on("end", function (status) {
      console.log(status);
    });

    return function cleanup() {
      stream.cancel();
    };
  }, [client]);

  return (
    <div className="status-header">
      <Row className="status-header-row">
        <Col s={4} l={3} offset="l1" className="status-header-col black-text center">
          <b>Chain Height: </b>
          {blockHeight}
        </Col>
        <Col s={4} l={3} offset="l1" className="status-header-col black-text center">
          <b>Mempool TX #: </b>
          {mempoolSize}
        </Col>
        <Col s={4} l={3} offset="l1" className="status-header-col black-text center">
          <b>Mempool Size: </b>
          {(mempoolBytes / 1024).toFixed(2)} kB
        </Col>
        {/* <Col s={4} className="black-text"><b>Transactions: </b>{this.state.transactions}</Col> */}
      </Row>
    </div>
  );
}

export default Live;
