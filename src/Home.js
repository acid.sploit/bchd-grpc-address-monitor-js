import React, { } from "react";
// import React, { useState, useEffect } from "react";
import { Container } from "react-materialize";
// import { Container, Row, Col } from "react-materialize";
import "./Home.css";

// import * as pb from "./bchrpc/bchrpc_pb_service";
// import {
//   GetMempoolInfoRequest,
//   GetBlockchainInfoRequest,
//   SubscribeTransactionsRequest,
//   TransactionFilter,
//   TransactionNotification,
//   SubscribeBlocksRequest,
//   BlockNotification,
// } from "./bchrpc/bchrpc_pb";

import { BrowserHeaders } from "browser-headers";
const headers = new BrowserHeaders();
headers.set("Access-Control-Allow-Origin", "*");
headers.set("Access-Control-Allow-Credentials", "true");
headers.set(
  "Access-Control-Allow-Headers",
  "Origin, Content-Type, Authorization, x-id, Content-Length, X-Requested-With, Date , Vary, Access-Control-Allow-Origin, Access-Control-Allow-Credentials"
);
headers.set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");

function Home(props) {
//   let server = props.server;
//   let client = props.client;

//   const [blockHeight, setBlockHeight] = useState(0);
//   const [mempoolSize, setMempoolSize] = useState(0);
//   const [mempoolBytes, setMempoolBytes] = useState(0);



//   useEffect(() => {
//         let filter = new TransactionFilter();
//         filter.setAllTransactions(true);
    
//         let subscribreTransactionRequest = new SubscribeTransactionsRequest();
//         subscribreTransactionRequest.setIncludeMempool(true);
//         subscribreTransactionRequest.setIncludeInBlock(false);
    
//         subscribreTransactionRequest.setSubscribe(filter);
    
//         let stream = client.subscribeTransactions(
//           subscribreTransactionRequest,
//           headers
//         );
    
//         stream.on("data", function (message) {
//           let tx = new TransactionNotification();
//           tx = message.toObject();
//           console.log(
//             "tx.unconfirmedTransaction.transaction.size",
//             tx.unconfirmedTransaction.transaction.size
//           );
    
//           setMempoolSize((mempoolSize) => mempoolSize + 1);
//           setMempoolBytes(
//             (mempoolBytes) =>
//               mempoolBytes + tx.unconfirmedTransaction.transaction.size
//           );
//         });
//         stream.on("status", function (status) {
//           console.log(status);
//         });
//         stream.on("end", function (status) {
//           console.log(status);
//         });

//     return function cleanup() {
//         stream.cancel();
//     };
//   }, [client]);

  return (
    <div>
      <Container>
        <h1 className="center">Home</h1>
      </Container>
    </div>
  );
}

export default Home;
