import React, {  } from 'react';
import './Search.css'
import { Row, Col, TextInput } from 'react-materialize'
import { useHistory } from 'react-router-dom';
import { decodeCashAddress, decodeCashAddressFormatWithoutPrefix } from '@bitauth/libauth'

function Search(props) {
  const history = useHistory();

  function handleSearchAddress(event) {
    
    event.preventDefault();
    let address = event.target.value;
    

    if (address.length === 0) {
      props.setSearchError("")
    } else {
      if (address.slice(0, 12) === 'bitcoincash:' || address.slice(0, 8) === 'testnet:') {
        let decodedCashAddress = decodeCashAddress(address);
        if (typeof (decodedCashAddress) === 'object') {
          address.slice(0, 12) === 'bitcoincash:' ? address = address.slice(12) : address = address.slice(8);
          let link = "/addr/" + address;

          props.setSearchError("");
          props.setSearchAddress(address);
          history.push(link);

        } else if (typeof (decodedCashAddress) === 'string') {
          props.setSearchError('Invalid address: ' + decodedCashAddress);
        }
      } else {
        let decodedCashAddress = decodeCashAddressFormatWithoutPrefix(address);
        if (typeof (decodedCashAddress) === 'object') {
          let link = "/addr/" + address;

          props.setSearchError("");
          props.setSearchAddress(address)
          history.push(link);

        } else if (typeof (decodedCashAddress) === 'string') {
          props.setSearchError('Invalid address: ' + decodedCashAddress);
        }
      }
    }
  }

  return (
    <div>
      <Row className="search-row">

        <Col s={12} m={6} l={6} offset="m3 l3" className="black-text mono-font">
          <TextInput
            className="search black-text mono-font"
            nolayout="true"
            icon="search"
            id="search"
            label="Search Address"
            placeholder={props.address}
            onChange={handleSearchAddress}
          />
          <span className="search-error">{props.searchError}</span>
        </Col>

      </Row>

    </div>
  );
}

export default Search;