import React, { Component } from "react";
import { Row, Col, Container } from "react-materialize";
// import 'materialize-css'
// import { decodeCashAddress, decodeCashAddressFormatWithoutPrefix } from '@bitauth/libauth'
import * as pb from "./bchrpc/bchrpc_pb_service";

import { HashRouter, Switch, Route } from "react-router-dom";

import "./App.css";
import Nav from "./Nav";
import Search from "./Search";
import Address from "./Address";
import History from "./History";
import Foot from "./Foot";
import Home from "./Home";

// import validateAddress from './utils';

const mainnet = [
  "https://bchd.sploit.cash",
  "https://bchd.greyh.at:8335",
  "https://bchd.imaginary.cash:8335",
  "https://ryzen.electroncash.de:8335",
  "https://bchd.fountainhead.cash",
  "https://bchd2.fountainhead.cash",
  "https://bchd3.prompt.cash:8335",

];
const testnet = [
  "https://ryzen.electroncash.de:18335",
  "https://bchd-testnet.electroncash.de:18335",
  "https://bchd-testnet.greyh.at:18335",
];

// const testnet_address = "qrws3fu6xldz3cn8ls59rjd3cc27ugduty48cgywy7";
// const mainnet_address = "qq5rlqcev6mgkhrhfvevr67yl8mtwqmmgsq0mmg6yg"

// const init_address = {
//     testnet: testnet_address,
//     mainnet: mainnet_address
// }

class App extends Component {
  constructor(props) {
    super(props);

    let server;

    if(localStorage.getItem("server")) {
        server = localStorage.getItem("server");
    } else {
        server = mainnet[0];
    }
    this.state = {
      // address: init_address.mainnet,
      address: "",
      label: "",
      searchError: "",
      networks: {
        mainnet: mainnet,
        testnet: testnet,
      },

      server: server,
      client: new pb.bchrpcClient(server),
    };

    this.handleServer = this.handleServer.bind(this);
    this.setSearchAddress = this.setSearchAddress.bind(this);
    this.setSearchError = this.setSearchError.bind(this);
    this.handleAddressLabel = this.handleAddressLabel.bind(this);
    this.hookLabel = this.hookLabel.bind(this);
  }

  setSearchAddress(address) {
    this.setState({ address: address });
  }

  setSearchError(error) {
    this.setState({ searchError: error });
  }

  handleServer(event) {
    event.preventDefault();
    console.log("server: " + event.target.value);
    this.setState({
      server: event.target.value,
      client: new pb.bchrpcClient(event.target.value),
    });

    localStorage.setItem("server", event.target.value)
  }

  handleAddressLabel(event) {
    event.preventDefault();
    let label = event.target.value;
    this.setState({ label: label });
  }

  hookLabel(label) {
    console.log("hookLabel:", label);
    this.setState({ label: label });
  }

  // handleSearchAddress(event) {
  //     event.preventDefault();
  //     let address = event.target.value;

  //     if (address.length === 0) {
  //         this.setSearchError("")
  //     } else {
  //         if (address.slice(0, 12) === 'bitcoincash:' || address.slice(0, 8) === 'testnet:') {
  //             let decodedCashAddress = decodeCashAddress(address);
  //             if (typeof (decodedCashAddress) === 'object') {
  //                 address.slice(0, 12) === 'bitcoincash:' ? address = address.slice(12) : address = address.slice(8);
  //                 let link = "/addr/" + address;

  //                 this.setSearchError("");
  //                 this.setState({ address: address });
  //                 history.push(link);

  //             } else if (typeof (decodedCashAddress) === 'string') {
  //                 this.setSearchError('Invalid address: ' + decodedCashAddress);
  //             }
  //         } else {
  //             let decodedCashAddress = decodeCashAddressFormatWithoutPrefix(address);
  //             if (typeof (decodedCashAddress) === 'object') {
  //                 let link = "/addr/" + address;

  //                 this.setSearchError("");
  //                 this.setState({ address: address });
  //                 history.push(link);

  //             } else if (typeof (decodedCashAddress) === 'string') {
  //                 this.setSearchError('Invalid address: ' + decodedCashAddress);
  //             }
  //         }
  //     }
  // }

  // validateAddress(address) {
  //     if (address.slice(0, 12) === 'bitcoincash:' || address.slice(0, 13) === 'simpleledger:' || address.slice(0, 8) === 'testnet:') {
  //         let decodedCashAddress = decodeCashAddress(address);
  //         if (typeof (decodedCashAddress) === 'object') {
  //             if (address.slice(0, 12) === 'bitcoincash:') {
  //                 address = address.slice(0, 12)
  //             }
  //             if (address.slice(0, 13) === 'simpleledger:') {
  //                 address = address.slice(0, 13)
  //             }
  //             if (address.slice(0, 8) === 'testnet:') {
  //                 address = address.slice(0, 8)
  //             }
  //             return {
  //                 isValid: true,
  //                 address: address,
  //                 error: "",
  //             }
  //         } else if (typeof (decodedCashAddress) === 'string') {
  //             return {
  //                 isValid: false,
  //                 address: "",
  //                 error: decodedCashAddress,
  //             }
  //         }
  //     } else {
  //         let decodedCashAddress = decodeCashAddressFormatWithoutPrefix(address);
  //         if (typeof (decodedCashAddress) === 'object') {
  //             return {
  //                 isValid: true,
  //                 address: address,
  //                 error: "",
  //             }

  //         } else if (typeof (decodedCashAddress) === 'string') {
  //             return {
  //                 isValid: false,
  //                 address: "",
  //                 error: decodedCashAddress,
  //             }
  //         }
  //     }
  // }

  render() {
    return (
      // [
      <HashRouter>
        <header>
          <Nav
            server={this.state.server}
            client={this.state.client}
            networks={this.state.networks}
            handleServer={this.handleServer}
          />
        </header>
        <main>
          <Row>
            <Col s={12} m={4} l={1}>
              <History
                address={this.state.address}
                label={this.state.label}
                hookLabel={this.hookLabel}
              />
            </Col>
            <Col s={12} m={8} l={11} offset="m4 l1">
              <Container>
                <Search
                  address={this.state.address}
                  searchError={this.state.searchError}
                  setSearchAddress={this.setSearchAddress}
                  setSearchError={this.setSearchError}
                />
                <Switch>
                  <Route
                    exact
                    path="/"
                    render={({ match }) => (
                      <Home
                        server={this.state.server}
                        client={this.state.client}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/addr/:addr"
                    render={({ match }) => (
                      <Address
                        address={match.params.addr}
                        label={this.state.label}
                        setSearchAddress={this.setSearchAddress}
                        setSearchError={this.setSearchError}
                        handleAddressLabel={this.handleAddressLabel}
                        server={this.state.server}
                        client={this.state.client}
                      />
                    )}
                  />
                </Switch>
              </Container>
            </Col>
          </Row>
        </main>
        <Foot />
      </HashRouter>
      // ]
    );
  }
}

export default App;
