import React, { PureComponent } from 'react';
import { Button, Icon } from 'react-materialize'
import { Link } from 'react-router-dom'
import './History.css'

class History extends PureComponent {
    constructor(props) {
        let addresses;
        let label = "";
        // localStorage.clear('addresses');
        if (localStorage.getItem('addresses')) {
            addresses = new Map(JSON.parse(localStorage.getItem('addresses')));
            if(addresses.has(props.address)){
                label = addresses.get(props.address)
                console.log("constructor:", props.address)
                props.hookLabel(label);
            }
        } else {
            addresses = new Map().set(props.address, "");
        }
        
        
        super(props);
        this.state = {
            address: props.address,
            addresses: addresses,
        };
        this.removeHistoryItem = this.removeHistoryItem.bind(this);

    }

    componentDidMount() {
        // this.props.hookLabel(this.state.addresses.get(this.state.address));

    }

    componentDidUpdate(prevProps) {
        if (this.props.address !== prevProps.address) {
            let addresses = this.state.addresses

            if (!addresses.has(this.props.address)) {
                addresses.set(this.props.address, "");

                this.setState({
                    address: this.props.address,
                    addresses: addresses,
                })

                localStorage.setItem('addresses', JSON.stringify([...addresses]));
            }
            console.log("fetch label:", this.props.address, this.props.label)
            this.props.hookLabel(this.state.addresses.get(this.props.address));
        }

        if (this.props.label !== prevProps.label) {
            let addresses = this.state.addresses
            if(addresses.has(this.props.address)) {
                addresses.set(this.props.address, this.props.label);
                localStorage.setItem('addresses', JSON.stringify([...addresses]));
                this.setState({addresses: new Map(addresses)});
            }
            console.log("update latel:", this.props.address, this.props.label)
            this.props.hookLabel(this.props.label);
        }

        // console.log("update:", this.props.address, this.props.label)
        // this.props.hookLabel(this.state.addresses.get(this.props.address));
    }

    removeHistoryItem(event) {
        let item = event.currentTarget.value;
        let history = this.state.addresses;

        console.log("remove history item: ", item);
        history.delete(item);
        localStorage.setItem('addresses', JSON.stringify([...history]));
        this.setState({addresses: new Map(history)})
    }

    render() {
        return (
            <div>
                <ul id="slide-out" className="sidenav sidenav-fixed z-depth-0">
                    <div className="title center">History</div>
                    <div className="divider"></div>
                    {[...this.state.addresses.keys()].reverse().map(address => {
                        let link = "/addr/" + address
                        return (
                            <div key={link}>
                                <Link  to={link} >
                                    <Button className="mono-font lowercase history-button"
                                        flat node="button"
                                        waves="light"
                                    // value={address}
                                    // onClick={this.props.handleHistory.bind(this)}
                                    >

                                        {this.state.addresses.get(address) !== "" ? this.state.addresses.get(address) : address.slice(0, 6) + "..." + address.slice(-6)}
                                    </Button>
                                </Link>
                                <Button
                                    className="history-remove"
                                    node="button"
                                    flat
                                    value={address}
                                    onClick={this.removeHistoryItem.bind(this)}
                                >
                                    <Icon tiny>remove</Icon>
                                </Button>
                            </div>
                        )
                    })}
                </ul>
                {/* sidenav trigger moved to Nav.js */}
                {/* <a href="#!" data-target="slide-out" className="sidenav-trigger hide-on-med-and-up"><i className="material-icons black-text">menu</i></a> */}

            </div>
        );
    }
}

export default History;